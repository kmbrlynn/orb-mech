#include <gd.h>

#include "engine.h"

void system_init(int argc, char *argv[], struct system *s)
{
    int body_list_flag = 0, body_args = 0;

    for (int iarg = 1; iarg < argc; iarg++) {
        char *c;
        if (strcmp("system_radius_meters", argv[iarg - 1]) == 0)
            s->radius_meters = strtod(argv[iarg], &c);
        
        if (strcmp("body_list", argv[iarg - 1]) == 0)
            body_list_flag = 1;

        if (body_list_flag)
            body_args++;  
    }

    s->B = (body_args + 1) / BODY_FIELDS;
    fprintf(stderr, "%s: %ld bodies in system with %5.4e m radius:\n",
        __func__, s->B, s->radius_meters);

    bodies_init(s, body_args, &(argv[argc - body_args]));

    fprintf(stderr, "\n");
}

void system_cleanup(struct system *s)
{
    free(s->bodies);
}

int main(int argc, char *argv[])
{
    struct system galaxy;

    system_init(argc, argv, &galaxy);

    system_cleanup(&galaxy);
    return 0;
}
