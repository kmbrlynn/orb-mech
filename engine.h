#ifndef ENGINE_H
#define ENGINE_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BODY_FIELDS 6

struct system
{
    size_t B;
    double radius_meters;
    struct body *bodies;
};

struct body
{
    char *name;
    double mass_kilograms;
    double xpos_meters;
    double ypos_meters;
    double xvel_meters_sec;
    double yvel_meters_sec;
};

void exit_with_errno(const char *caller);

void bodies_init(struct system *s, int bodies_argc, char *bodies_argv[]);

#endif
