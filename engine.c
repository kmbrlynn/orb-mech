#include "engine.h"

void exit_with_errno(const char *caller)
{
    fprintf(stderr, "%s: %s\n", caller, strerror(errno));
    exit(EXIT_FAILURE);
}

void bodies_init(struct system *s, int bodies_argc, char *bodies_argv[])
{
    s->bodies = calloc(s->B, sizeof(struct body));
    if (s->bodies == NULL)
        exit_with_errno(__func__);

    for (int b = 0, iarg = 0; iarg < bodies_argc; iarg += BODY_FIELDS, b++) {
        char *c;
        s->bodies[b].name = bodies_argv[iarg];
        s->bodies[b].mass_kilograms = strtod(bodies_argv[iarg + 1], &c);
        s->bodies[b].xpos_meters = strtod(bodies_argv[iarg + 2], &c);
        s->bodies[b].ypos_meters = strtod(bodies_argv[iarg + 3], &c);
        s->bodies[b].xvel_meters_sec = strtod(bodies_argv[iarg + 4], &c);
        s->bodies[b].yvel_meters_sec = strtod(bodies_argv[iarg + 5], &c);

        fprintf(stderr, "%s: %s is %5.4e kg, located at %5.4e,%5.4e m with starting velocity %5.4e m/s x, %5.4e m/s y\n", 
            __func__, s->bodies[b].name, s->bodies[b].mass_kilograms,
            s->bodies[b].xpos_meters, s->bodies[b].ypos_meters,
            s->bodies[b].xvel_meters_sec, s->bodies[b].yvel_meters_sec);
    }
}
