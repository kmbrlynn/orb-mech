# orb-mech

Simple 2D orbital mechanics generator

##### References

- https://nssdc.gsfc.nasa.gov/planetary/factsheet/

##### Build

```console
$ apt install make valgrind libgd-dev
$ make -j
$ make check
```

##### Run

```console
$ cat inner_planets.txt | xargs ./orb_mech
```