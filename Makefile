CFLAGS = -Wall -g
LDFLAGS = -lgd

all: orb-mech

orb-mech: orb-mech.c engine.o
	${CC} ${CFLAGS} -o $@ $^ ${LDFLAGS}

%.o: %.c
	${CC} ${CFLAGS} -o $@ $^ -c

check:
	@valgrind --tool=memcheck --show-leak-kinds=all \
		./orb-mech `cat inner_planets.txt | xargs`

clean:
	rm -rf orb-mech *.o
